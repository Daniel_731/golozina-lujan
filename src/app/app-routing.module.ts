import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {OfferPageComponent} from './pages/offer-page/offer-page.component';
import {ProductPageComponent} from './pages/product-page/product-page.component';
import {CigarettePageComponent} from './pages/cigarette-page/cigarette-page.component';
import {HistoryPageComponent} from './pages/history-page/history-page.component';
import { OrdersPageComponent } from './pages/orders-page/orders-page.component';


const routes: Routes = [
   // Home
   {
      path      : '',
      component : OfferPageComponent
   },

   // Offer
   {
      path      : 'offer',
      component : OfferPageComponent
   },

   // Product
   {
      path      : 'product',
      component : ProductPageComponent
   },

   {
      path      : 'product/:filter',
      component : ProductPageComponent
   },

   // Cigarette
   {
      path      : 'cigarette',
      component : CigarettePageComponent
   },

   // History
   {
      path      : 'history',
      component : HistoryPageComponent
   },

   {
      path      : 'orders',
      component : OrdersPageComponent
   },

   
];

@NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule]
})
export class AppRoutingModule {
}
