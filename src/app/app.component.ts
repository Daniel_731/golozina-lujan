import { Component } from '@angular/core';


declare const SA: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'golozina-lujan';

  constructor() {
    //SA.redirection_mobile();
  }

  ngOnInit() {
    //por defecto va a http://m.golozinalujan.com
    SA.redirection_mobile();
  }
}
