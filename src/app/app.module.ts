import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {AsideMenuComponent} from './components/aside-menu/aside-menu.component';
import {OfferPageComponent} from './pages/offer-page/offer-page.component';
import {ProductPageComponent} from './pages/product-page/product-page.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { CigarettePageComponent } from './pages/cigarette-page/cigarette-page.component';
import { HistoryPageComponent } from './pages/history-page/history-page.component';
import { SumPipe } from './pipes/sum.pipe';
import { environment } from 'src/environments/environment';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { NgxUiLoaderModule, POSITION, SPINNER, PB_DIRECTION, NgxUiLoaderConfig } from  'ngx-ui-loader';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { OrdersPageComponent } from './pages/orders-page/orders-page.component';
//import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';


const ngxUiLoaderConfig: NgxUiLoaderConfig = {
   bgsColor: 'red',
   bgsPosition: POSITION.bottomRight,
   bgsSize: 70,
   bgsType: SPINNER.rectangleBounce, // background spinner type
   fgsType: SPINNER.chasingDots, // foreground spinner type
   pbDirection: PB_DIRECTION.leftToRight, // progress bar direction
 };

@NgModule({
   declarations: [
      AppComponent,
      HeaderComponent,
      AsideMenuComponent,
      OfferPageComponent,
      ProductPageComponent,
      CigarettePageComponent,
      HistoryPageComponent,
      SumPipe,
      LoginComponent,
      RegisterComponent,
      OrdersPageComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      FontAwesomeModule,
      FormsModule,
      ReactiveFormsModule,
      NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
      AngularFireModule.initializeApp(environment.firebase),
      AngularFirestoreModule, // imports firebase/firestore, only needed for database features
      AngularFireAuthModule, // imports firebase/auth, only needed for auth features
      BrowserAnimationsModule, // required animations module
      ToastrModule.forRoot({
         timeOut: 2000,
         preventDuplicates: true,
         closeButton:true
       }), // ToastrModule added
      InfiniteScrollModule
   ],
   providers: [
     {provide: LocationStrategy, useClass: HashLocationStrategy},
  
    ],
   bootstrap: [AppComponent]
})
export class AppModule {
}

//platformBrowserDynamic().bootstrapModule(AppModule);