import { Component, OnInit } from '@angular/core';
import { SubCategoriaService } from 'src/app/services/sub-categoria/sub-categoria.service';
import { CategoriaDTO } from 'src/models/CategoriaDTO';
import { SubCategoriaDTO } from 'src/models/SubCategoriaDTO';
import { CategoriaAccordionDTO } from 'src/models/CategoriaAccordionDTO';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aside-menu',
  templateUrl: './aside-menu.component.html',
  styleUrls: ['./aside-menu.component.scss']
})
export class AsideMenuComponent implements OnInit {

  listCategorias: CategoriaDTO[];
  listSubCategorias: SubCategoriaDTO[];
  listcategoriasAccordion: CategoriaAccordionDTO[];

  CIGARRILLOS:string = 'CIGARRILLOS'
  
  constructor(private router: Router,
    private _subCategoriaService: SubCategoriaService,
    private _categoriaService: CategoriaService) {  
  }

  ngOnInit() {
    this.initMenuRubros();
  }
  async initMenuRubros() {
    await this.findCategorias();
    await this.findSubCategorias();
    await this.organizarObjetoAccordion();
  }
  
  async findSubCategorias() {
    this.listSubCategorias = [];
    let data = await this._subCategoriaService.findSubCategorias()
    if (data != null && data.length > 0) {
      this.listSubCategorias = data;
    } 
  }

  async findCategorias() {
    this.listCategorias = [];
    let data = await this._categoriaService.findCategorias()
    if (data != null && data.length > 0) {
      this.listCategorias = data;
    }
  }

  async organizarObjetoAccordion() {
    this.listcategoriasAccordion = [];
    let categoriaCigarillo: CategoriaAccordionDTO;

    this.listCategorias.forEach(element => {
      if (element.habilitado == true) {
        let categoriasAccordion = new CategoriaAccordionDTO();
        categoriasAccordion.ListSubCategorias = [];
        categoriasAccordion.categoriaFoto = element.foto;
        categoriasAccordion.categoriaId = element.id;
        categoriasAccordion.categoriaNombre = element.nombre;

        if(element.id == this.CIGARRILLOS){
          categoriaCigarillo = categoriasAccordion;
        }

        this.listSubCategorias.forEach(subCategoria => {
          if (subCategoria.habilitado == true) {
            if (subCategoria.idCategoria == element.id) {
              categoriasAccordion.ListSubCategorias.push(subCategoria);
            }
          }
        });
        this.listcategoriasAccordion.push(categoriasAccordion);
      }
    });

    //delete item CIGARRILLOS
    this.listcategoriasAccordion = this.listcategoriasAccordion.filter(function (value) {
      return value.categoriaId != this.CIGARRILLOS;
    });    

    //insert item CIGARRILLOS en la posicion 0
    this.listcategoriasAccordion.splice(0,0,categoriaCigarillo);
  }

  goProducts(subCategoria:SubCategoriaDTO){
    this.router.navigate(["/product", { subcategoria: subCategoria.id}]);
  }

}
