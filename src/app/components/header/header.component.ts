import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { faTags, faEdit, faStore, faFileInvoice, faHistory, faSearch, faShoppingCart, faSignInAlt,faUser } from '@fortawesome/free-solid-svg-icons';
import { FormControl } from '@angular/forms';
import { debounceTime, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AlmacenamientoLocalService } from 'src/app/services/almacenamiento-local/almacenamiento-local.service';
import { datosAL } from 'src/models/datosAL';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AutenticacionFirebaseService } from 'src/app/services/auntenticacion-firebase/autenticacion-firebase.service';
import { UsuarioDTO, PerfilUsuario } from 'src/models/UsuarioDTO';
import { ProductShoppingCartDTO } from 'src/models/ProductShoppingCartDTO';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { ToastrService } from 'ngx-toastr';

@Component({
   selector: 'app-header',
   templateUrl: './header.component.html',
   styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

   faTags = faTags;
   faEdit = faEdit;
   faStore = faStore;
   faFileInvoice = faFileInvoice;
   faHistory = faHistory;
   faSearch = faSearch;
   faShoppingCart = faShoppingCart;
   faSignInAlt = faSignInAlt;
   faUser = faUser;

   queryField: FormControl = new FormControl();

   @ViewChild('closeModalLogin', { static: false }) closeModalLogin: ElementRef;
   @ViewChild('closeModalRegister', { static: false }) closeModalRegister: ElementRef;


   listProductosCarro: ProductShoppingCartDTO[] = [];
   usuario: UsuarioDTO;
   usuarioPuedePedir: boolean;

   constructor(private router: Router,
      public _almacenamientoLocalService: AlmacenamientoLocalService,
      private ngxService: NgxUiLoaderService,
      private toastr: ToastrService,
      public _autenticacionFirebaseService: AutenticacionFirebaseService,
      public _usuarioService: UsuarioService) { }

   ngOnInit() {
      this.verificarSesion();
      this.searchFilter();
      this.getUser();
   }

   searchFilter() {
      this.queryField.valueChanges.pipe(
         debounceTime(500),
         switchMap(filter => {
            //console.log(filter);
            this.router.navigate(["/product", { filter: filter }]);
            return filter;
         })
      ).subscribe(val => {
         //console.log(val)
      });
   }

   /**
    * permite identificar si existen datos localmente, si existen sobreescribre los datos en
    * el locales para poder hidrar las variables globales
    */
   async verificarSesion() {
      let datosSesion: datosAL = await this._almacenamientoLocalService.obtenerDatosSesion();
      if (datosSesion != null && datosSesion.idUsuario != null) {
         this._almacenamientoLocalService.guardarDatosAL(datosSesion);
      }
      else {
         this._almacenamientoLocalService.limpiarTodoAL();
      }
   }

   async salir() {
      await this.ngxService.start();
      await this._autenticacionFirebaseService.cerrarSesion();
      await this.verificarSesion();
      this.router.navigate(["/product", { sesion: false }]);
      this.listProductosCarro = [];
      this.ngxService.stop();
   }

   closeDialogLoginEvent() {
      this.closeModalLogin.nativeElement.click()
      this.getUser();
      this.router.navigate(["/product", { sesion: true }]);
   }

   closeDialogRegisterEvent() {
      this.closeModalRegister.nativeElement.click()
      this.getUser();
      this.router.navigate(["/product", { sesion: true }]);
   }

   getUser() {
      if(this._almacenamientoLocalService.idUsuario != null){ //cuando se va a subscribe
        this._usuarioService.getUser().subscribe(result => {
         if(this._almacenamientoLocalService.idUsuario == null || 
            this._almacenamientoLocalService.idUsuario == undefined ){ //cuando se ya esta subscribe
            this.usuario=null;
            this.listProductosCarro = [];
            return;
          }
          if(result != null && result.length == 1){
            this.usuario = result[0]
            this.validarUsuario();
            this.listProductosCarro = this.usuario.productsCart;
            console.log(this.usuario);
          }
        });
      }
    }

    async showItemCard(){
      let datosSesion: datosAL = await this._almacenamientoLocalService.obtenerDatosSesion();
      if (datosSesion != null && datosSesion.idUsuario != null) {
         let items:number = this.listProductosCarro != null && this.listProductosCarro != undefined?
          this.listProductosCarro.length : 0;
         this.router.navigate(["/orders"]);
      }else{
        console.log("tal vez ir a login");
      }
    }

    /** 
   * metodo que permite establecer si el cliente o del vendedor (el vendedor siempre puede) puede hacer pedidos.
  */
 validarUsuario() {
   if (this.usuario != null && this.usuario.perfil == PerfilUsuario._perfil_vendedor.toString()) { //el vendedor siempre puede pedir
     this.usuarioPuedePedir = true;
     //this.tipoPrecioUsuario = this._almacenamientoLocalService.clienteTemporal.tipoPrecio;
   }
   else if (this.usuario != null &&
     this.usuario.perfil == PerfilUsuario._perfil_cliente.toString() &&
     this.usuario.numeroCliente != null &&
     this.usuario.numeroCliente > 0) { //el usuario puede pedir siempre y cuando este aprobado
     this.usuarioPuedePedir = true;
     //this.tipoPrecioUsuario = this.usuario.tipoPrecio;
   }
   console.log(this.usuarioPuedePedir);
   
 }
}
