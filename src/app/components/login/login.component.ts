import { Component, OnInit, ViewChild, ElementRef, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { datosAL } from 'src/models/datosAL';
import { AlmacenamientoLocalService } from 'src/app/services/almacenamiento-local/almacenamiento-local.service';
import { AutenticacionFirebaseService } from 'src/app/services/auntenticacion-firebase/autenticacion-firebase.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  //start dialog
  @Input()
  inDialog: boolean = false;
  
  @Output()
  //closeDialogEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  public closeDialogEvent = new EventEmitter<void>();
  //end dialog 

  @Input()
  inFrom: boolean = false;

  constructor(private _fb: FormBuilder,
              private ngxService: NgxUiLoaderService,
              private toastr: ToastrService,
              public _autenticacionFirebaseService: AutenticacionFirebaseService,
              public _usuarioService: UsuarioService,
              public _almacenamientoLocalService: AlmacenamientoLocalService) { 

  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.loginForm = this._fb.group({
      inputEmail: ['', Validators.compose([Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required])],
      inputPassword: ['', Validators.compose([Validators.required])],
    });
  }
  // go to register page
  register() {
    //this.nav.push('RegisterPage', { 'usuario':null });
  }

  login() {

    let correo: string = this.loginForm.value.inputEmail;
    let clave: string = this.loginForm.value.inputPassword;

    this.ngxService.start();

    this.iniciarSesionConEmail(correo, clave).then(() => {
      this.ngxService.stop();
    },
      (error) => {
        this.ngxService.stop();
        this.toastr.warning(error,"Ocurrio algo");
        console.error(error);
      })

  }

  async iniciarSesionConEmail(correo: string, clave: string): Promise<void> {

    try {
      //se autentica el usuario en firebase con email
      let sesionCorrecta: boolean = await this._autenticacionFirebaseService.iniciarSesionConEmail(correo, clave);

      if (sesionCorrecta == true){
        //consulto el usario autenticado para guardar los datos en la sesion.
        let itemUsuario = await this._usuarioService.consultarUsuarioPorEmail(correo);
        if (itemUsuario == null) {
          throw ("No se encontro el usuario");
        }

        let usuarioLogin = itemUsuario;
        // guardo los datos en la sesion
        let datosal = new datosAL()
        datosal.idUsuario = usuarioLogin.id;
        datosal.correoUsuario = usuarioLogin.correo;
        datosal.perfilUsuario = usuarioLogin.perfil;
        datosal.nombreCompleto = usuarioLogin.nombreCompleto;
        datosal.telefono = usuarioLogin.telefono;
        let almacenamientoCorrecto: boolean = await this._almacenamientoLocalService.guardarDatosAL(datosal);

        if (!almacenamientoCorrecto) {
          throw ("Con el almacenamiento local");
        }

        if(this.inDialog){
          this.closeDialogEvent.emit();
        }
        this.loginForm.reset();
      }
      
    } catch (error) {
      throw new Error(error);
    }

  }

  forgotPass() {
    
    /*let forgot = this.forgotCtrl.create({
      title: '¿Se te olvidó tu contraseña?',
      message: "Ingrese su dirección de correo electrónico para enviar un enlace.",
      inputs: [
        {
          name: 'correo',
          placeholder: 'correo electrónico',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enviar',
          handler: data => {

            this._autenticacionFirebaseService.restrablecerContrasena(data.correo).
            then((res : boolean) =>{
              if(res == true){
                this._util.presentarToast("El correo electrónico fue enviado con éxito.");
              }
            }).catch(() =>{
              this._util.presentarToast("Error, verifique el correo electrónico.");
            });  
          }
        }
      ]
    });
    forgot.present();*/
  }

  checkErrors(control: string): boolean {
    return this.loginForm.controls[control].invalid
      && (this.loginForm.controls[control].dirty || this.loginForm.controls[control].touched);
  }

}
