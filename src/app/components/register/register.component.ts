import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { AutenticacionFirebaseService } from 'src/app/services/auntenticacion-firebase/autenticacion-firebase.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { AlmacenamientoLocalService } from 'src/app/services/almacenamiento-local/almacenamiento-local.service';
import { PerfilUsuario, UsuarioDTO } from 'src/models/UsuarioDTO';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public formulario: FormGroup;

  //start dialog
  @Input()
  inDialog: boolean = false;
  
  @Output()
  public closeDialogEvent = new EventEmitter<void>();
  //end dialog 

  constructor(private _fb: FormBuilder,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    public _autenticacionFirebaseService: AutenticacionFirebaseService,
    public _usuarioService: UsuarioService,
    public _almacenamientoLocalService: AlmacenamientoLocalService) {

  }

  ngOnInit() {
    this.formulario = this._fb.group({
      txtNombreCompleto: ['', Validators.required],
      //txtPrecio: [null],
      //txtNumCliente: [null],
      txtCorreo: ['', Validators.compose([Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required])],
      txtClave: ['', Validators.required],
      txtCedula: [''],
      txtTelefono: ['', Validators.required],
      txtDireccion: ['', Validators.required],
      txtLocalidad: ['', Validators.required]
    });
    this.formulario.valueChanges.subscribe((v) => {
      console.log(this.formulario.valid)
    });
  }

  register() {
    if (!this.formulario.valid) { return; }

    let UsuarioGuardar: UsuarioDTO = {
      id: null,
      nombreCompleto: this.formulario.value.txtNombreCompleto,
      correo: this.formulario.value.txtCorreo,
      contrasena: this.formulario.value.txtClave,
      cuit: this.formulario.value.txtCedula,
      telefono: this.formulario.value.txtTelefono,
      direccion: this.formulario.value.txtDireccion,
      localidad: this.formulario.value.txtLocalidad,
      perfil: PerfilUsuario._perfil_cliente.toString(),
      habilitado: true,
      foto: null,
      nombreFoto: null,
      numeroCliente: null,
      tipoPrecio: null,
      productsCart: []
    }

    this.ngxService.start();

    this._usuarioService.registrarUsuario(UsuarioGuardar).then((respuesta) => {
      this.ngxService.stop();
      this.toastr.success("Su solicitud de registro a sido enviada.");
      if(this.inDialog){
        this.closeDialogEvent.emit();
      }
    },
      (error) => {
        this.ngxService.stop();
        this.toastr.warning(error,"Ocurrio algo");
        console.error(error);
      });
  }

  checkErrors(control: string): boolean {
    return this.formulario.controls[control].invalid
      && (this.formulario.controls[control].dirty || this.formulario.controls[control].touched);
  }

}


