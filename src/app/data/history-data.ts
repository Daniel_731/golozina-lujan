export const HISTORY = [
   {
      id_client   : 1,
      order       : 293874,
      date        : '17/10/2019',
      client      : 'Fulanito de tal',
      articles    : [
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         }
      ]
   },
   {
      id_client   : 2,
      order       : 293875,
      date        : '17/10/2019',
      client      : 'Fulanito de tal',
      articles    : [
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         }
      ]
   },
   {
      id_client   : 3,
      order       : 293876,
      date        : '17/10/2019',
      client      : 'Fulanito de tal',
      articles    : [
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         }
      ]
   },
   {
      id_client   : 4,
      order       : 293877,
      date        : '17/10/2019',
      client      : 'Fulanito de tal',
      articles    : [
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         }
      ]
   },
   {
      id_client   : 5,
      order       : 293878,
      date        : '17/10/2019',
      client      : 'Fulanito de tal',
      articles    : [
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         }
      ]
   },
   {
      id_client   : 6,
      order       : 293879,
      date        : '17/10/2019',
      client      : 'Fulanito de tal',
      articles    : [
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         },
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         },
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         },
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         },
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         },
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         },
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         },
         {
            idArticle   : 1,
            product     : 'Dulce de calabaza',
            price       : 1000,
            quantity    : 1
         },
         {
            idArticle   : 2,
            product     : 'Cocadas',
            price       : 2000,
            quantity    : 2
         },
         {
            idArticle   : 3,
            product     : 'Alfajor de coco',
            price       : 3000,
            quantity    : 3
         },
         {
            idArticle   : 4,
            product     : 'Higos',
            price       : 4000,
            quantity    : 4
         }
      ]
   }
];
