export const PRODUCTS = [
   {
      id       : 1,
      img      : 'assets/product-page/product_1.jpeg',
      name     : 'Producto 1',
      price    : 1100,
      category : 1
   },
   {
      id       : 2,
      img      : 'assets/product-page/product_2.jpeg',
      name     : 'Producto 2',
      price    : 1200,
      category : 1
   },
   {
      id       : 3,
      img      : 'assets/product-page/product_3.jpeg',
      name     : 'Producto 3',
      price    : 1300,
      category : 1
   },
   {
      id       : 4,
      img      : 'assets/product-page/product_4.jpeg',
      name     : 'Producto 4',
      price    : 1400,
      category : 1
   },
   {
      id       : 5,
      img      : 'assets/product-page/product_5.jpeg',
      name     : 'Producto 5',
      price    : 1500,
      category : 2
   },
   {
      id       : 6,
      img      : 'assets/product-page/product_6.jpeg',
      name     : 'Producto 6',
      price    : 1600,
      category : 2
   },

   {
      id       : 7,
      img      : 'assets/product-page/product_7.jpeg',
      name     : 'Producto 7',
      price    : 7700,
      category : 7
   },
   {
      id       : 3,
      img      : 'assets/product-page/product_2.jpeg',
      name     : 'Producto 3',
      price    : 7300,
      category : 7
   },
   {
      id       : 4,
      img      : 'assets/product-page/product_3.jpeg',
      name     : 'Producto 4',
      price    : 7300,
      category : 7
   },
   {
      id       : 5,
      img      : 'assets/product-page/product_4.jpeg',
      name     : 'Producto 5',
      price    : 7400,
      category : 7
   },
   {
      id       : 7,
      img      : 'assets/product-page/product_5.jpeg',
      name     : 'Producto 7',
      price    : 7500,
      category : 2
   },
   {
      id       : 8,
      img      : 'assets/product-page/product_6.jpeg',
      name     : 'Producto 8',
      price    : 7600,
      category : 2
   },




];
