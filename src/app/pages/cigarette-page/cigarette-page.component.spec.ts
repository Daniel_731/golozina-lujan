import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CigarettePageComponent } from './cigarette-page.component';

describe('CigarettePageComponent', () => {
  let component: CigarettePageComponent;
  let fixture: ComponentFixture<CigarettePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CigarettePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CigarettePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
