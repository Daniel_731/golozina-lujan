import {Component, OnInit} from '@angular/core';
import {PRODUCTS} from '../../data/productos-data';
import {faCartPlus} from '@fortawesome/free-solid-svg-icons';

@Component({
   selector: 'app-cigarette-page',
   templateUrl: './cigarette-page.component.html',
   styleUrls: ['./cigarette-page.component.scss']
})
export class CigarettePageComponent implements OnInit {

   private CATEGORY_CIGARETTE = 2;

   products: Array<object>;

   faCartPlus = faCartPlus;



   constructor() {}



   async ngOnInit() {
      this.products = await PRODUCTS.filter(value => value.category === this.CATEGORY_CIGARETTE);
   }

}
