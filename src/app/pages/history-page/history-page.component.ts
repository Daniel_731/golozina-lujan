import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { faFileInvoiceDollar } from '@fortawesome/free-solid-svg-icons';
import { PedidoDTO } from 'src/models/PedidoDTO';
import { datosAL } from 'src/models/datosAL';
import { AlmacenamientoLocalService } from 'src/app/services/almacenamiento-local/almacenamiento-local.service';
import { PedidosService } from 'src/app/services/pedidos/pedidos.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { PedidoDetalleDTO } from 'src/models/PedidoDetalleDTO';

@Component({
   selector: 'app-history-page',
   templateUrl: './history-page.component.html',
   styleUrls: ['./history-page.component.scss']
})
export class HistoryPageComponent implements OnInit {

   faFileInvoiceDollar = faFileInvoiceDollar;
   momentjs;
   modalIndex;


   total: number = 0;
   listPedidos: PedidoDTO[] = []
   listProductosPedido: PedidoDetalleDTO[] = [];

   @ViewChild('openModalDetalles', { static: false }) openModalDetalles: ElementRef;


   constructor(public _almacenamientoLocalService: AlmacenamientoLocalService,
      public _pedidosService: PedidosService,
      private ngxService: NgxUiLoaderService,
      private toastr: ToastrService) {
      moment.locale('es');
      this.momentjs = moment;
   }

   ngOnInit() {
      this.consultarPedidosPorCliente();
   }

   async consultarPedidosPorCliente() {

      this.ngxService.start();
      try {

         let datosAl: datosAL = await this._almacenamientoLocalService.obtenerDatosSesion();
         if (datosAl && datosAl.idUsuario) {
            let listPedidosCliente: PedidoDTO[] = await this._pedidosService.consultarPedidosPorCliente(datosAl.idUsuario);
            if (listPedidosCliente != null && listPedidosCliente.length > 0) {
               for (let item of listPedidosCliente) {
                  //let fechaEvento = moment.unix(Number(item.fechaPedido)).format("DD MMM h:mm a");
                  let fechaEvento = moment.unix(Number(item.fechaPedido)).format("LLLL a");
                  item['fechaPedidoConver'] = fechaEvento;
               }

               //se ordenan por fecha
               listPedidosCliente.sort(function (x, y) {
                  return Number(y.fechaPedido) - Number(x.fechaPedido);
               })
               this.listPedidos = listPedidosCliente;
               this.ngxService.stop();
            }
         }
      } catch (error) {
         this.ngxService.stop();
         this.toastr.error(error, "Ocurrio algo");
      }
   }

   verDetallePedido(pedido: PedidoDTO): void {
      this.consultarDetallePedido(pedido)
   }

   async consultarDetallePedido(pedido: PedidoDTO) {

      this.ngxService.start();
      this.openModalDetalles.nativeElement.click()

      try {
         this.listProductosPedido = [];

         let detallePedido = await this._pedidosService.obtenerDetallesPedido(pedido.id);
         if (detallePedido != null && detallePedido.length > 0) {
            this.listProductosPedido = detallePedido;
            this.calcularTotal();
         }
         this.ngxService.stop();

      } catch (error) {
         this.ngxService.stop();
         this.toastr.error(error, "Ocurrio algo");
      }
   }

   calcularTotal() {
      this.total = 0;
      for (let item of this.listProductosPedido) {
         let totalItem: number = item.precio * Number(item['cantidad']);
         this.total = Math.round((this.total + totalItem) * 100) / 100;
      }
   }
}
