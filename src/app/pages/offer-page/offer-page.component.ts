import { Component, OnInit } from '@angular/core';
import { CategoriaOfertaService } from 'src/app/services/categoria-oferta/categoria-oferta.service';
import { CategoriaOfertaDTO } from 'src/models/CategoriaOfertaDTO';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router } from '@angular/router';

@Component({
  selector: 'app-offer-page',
  templateUrl: './offer-page.component.html',
  styleUrls: ['./offer-page.component.scss']
})
export class OfferPageComponent implements OnInit {

   offers: Array<CategoriaOfertaDTO>;
   constructor(public _categoriaOfertaService : CategoriaOfertaService,
    private ngxService: NgxUiLoaderService,
    private router: Router) {

   }
   
   async ngOnInit() {
     this.ngxService.startBackground();
     await this.findCategoriasOferta();
     this.ngxService.stopBackground();
   }


   async findCategoriasOferta() {
     console.log("consulting");
      this.offers = [];
      let data = await this._categoriaOfertaService.findCategoriasOferta();
      if (data != null && data.length > 0) {
         this.offers = data;
      } 
    }

    goProducts(offer:CategoriaOfertaDTO){
      this.router.navigate(["/product", { categoriaOferta: offer.id }]);
    }

}
