import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AlmacenamientoLocalService } from 'src/app/services/almacenamiento-local/almacenamiento-local.service';
import { ToastrService } from 'ngx-toastr';
import { AutenticacionFirebaseService } from 'src/app/services/auntenticacion-firebase/autenticacion-firebase.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { ProductShoppingCartDTO } from 'src/models/ProductShoppingCartDTO';
import { UsuarioDTO } from 'src/models/UsuarioDTO';
import { FullScreenViewer } from 'iv-viewer';
import { ConfiguracionService } from 'src/app/services/configuracion/configuracion.service';
import { Configuracion } from 'src/models/Configuracion';
import { PedidoDTO, EstadoPedido } from 'src/models/PedidoDTO';
import { PedidoDetalleDTO } from 'src/models/PedidoDetalleDTO';
import { PedidosService } from 'src/app/services/pedidos/pedidos.service';

@Component({
  selector: 'app-orders-page',
  templateUrl: './orders-page.component.html',
  styleUrls: ['./orders-page.component.scss']
})
export class OrdersPageComponent implements OnInit {

  listProductosCarro: ProductShoppingCartDTO[] = [];
  usuario: UsuarioDTO; // puede ser un cliente o un vendedor
  subtotal: number = 0;
  descuento: number = 0;
  private configuracion: Configuracion;
  public observacionesPedido: string = null;

  constructor(public _almacenamientoLocalService: AlmacenamientoLocalService,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    public _autenticacionFirebaseService: AutenticacionFirebaseService,
    public _usuarioService: UsuarioService,
    public _configuracionService: ConfiguracionService,
    public _pedidosService: PedidosService) { }

  ngOnInit() {
    this.consultarConfiguracion();
    this.getUser();
  }

  getUser() {
    if (this._almacenamientoLocalService.idUsuario != null) { //cuando se va a subscribe
      this._usuarioService.getUser().subscribe(result => {
        if (this._almacenamientoLocalService.idUsuario == null ||
          this._almacenamientoLocalService.idUsuario == undefined) { //cuando se ya esta subscribe
          this.listProductosCarro = [];
          return;
        }
        if (result != null && result.length == 1) {
          this.usuario = result[0];
          this.listProductosCarro = result[0].productsCart;
          this.calcularSubtotal()
        }
      });
    }
  }

  async consultarConfiguracion() {
    let data = await this._configuracionService.findConfig();
    if (data != null && data.length > 0) {
      this.configuracion = data[0];
    } else {
      this.configuracion = {
        titulomensaje: 'Gracias.',
        mensaje: 'recibirás tu pedido pronto.'
      }
      console.warn("Sin datos");
    }
  }

  async changeCantidad(event, product: ProductShoppingCartDTO, index) {

    try {
      if (product.cantidad == null || product.cantidad == undefined || product.cantidad == 0) {
        this.toastr.warning("Establece la cantidad.");
        return;
      }
      this.usuario['productsCart'] = this.listProductosCarro;
      await this._usuarioService.actualizarUsuario(this.usuario);
      await this.calcularSubtotal()
    } catch (error) {
      this.toastr.error(error);
    }
  }

  async eliminarProducto(index, producto: ProductShoppingCartDTO) {

    console.log(index);

    this.listProductosCarro.splice(index, 1);

    this.usuario['productsCart'] = this.listProductosCarro;
    await this._usuarioService.actualizarUsuario(this.usuario);
    this.calcularSubtotal();
  }

  async calcularSubtotal() {
    this.subtotal = 0;
    if (this.listProductosCarro != null && this.listProductosCarro.length > 0) {
      for (let item of this.listProductosCarro) {
        let subtotalItem: number = item.producto.precio * Number(item['cantidad']);
        this.subtotal = Math.round((this.subtotal + subtotalItem) * 100) / 100;
      }
      console.log("this.subtotal ", this.subtotal);
    }
  }

  showImageFull(image) {
    const viewer = new FullScreenViewer();
    viewer.show(image.src);
  }

  public convertirFechaUnixtime(fecha: Date): string {
    return Math.floor(fecha.getTime() / 1000).toString();
  }

  public convertirUnixtimeFechaUnixtime(unixtime: number): Date {
    return new Date(unixtime * 1000);
  }

  async realizarPedido() {
    this.ngxService.start();

    try {

      if (this.listProductosCarro == null || this.listProductosCarro.length == 0) {
        this.toastr.warning("El carro de compras no tiene productos.");
        this.ngxService.stop();
        return;
      }

      //encabezado de pedido
      let encabezadoPedido: PedidoDTO;

      encabezadoPedido = {
        idCliente: this.usuario.id,
        numCliente: this.usuario.numeroCliente,
        razonSocialCliente: this.usuario.nombreCompleto,
        direccionCliente: this.usuario.direccion,
        telefonoCliente: this.usuario.telefono,
        correoCliente: this.usuario.correo,
        localidadCliente: this.usuario.localidad,
        fechaPedido: this.convertirFechaUnixtime(new Date()),
        estadoPedido: EstadoPedido._pedido_porprocesar.toString(),
        leido: false,
        observaciones: this.observacionesPedido,
        eliminado: false
      }

      //detalle de pediddo
      let listDetallePedido: PedidoDetalleDTO[] = [];
      for (let item of this.listProductosCarro) {
        let itemDetalle: PedidoDetalleDTO = {
          idProducto: item.producto.id,
          idPedido: null,
          codigoProducto: item.producto.codigo,
          nombreProducto: item.producto.nombre,
          descripcionProducto: item.producto.descripcion,
          fotoProducto: item.producto.foto,
          cantidad: Number(item['cantidad']),
          precio: item.producto.precio
        }
        listDetallePedido.push(itemDetalle);
      }

      let respuesta = await this._pedidosService.crearPedido(encabezadoPedido, listDetallePedido);

      if (respuesta == true) {
        //se eliminan los elementos en el carro de compras
        this.usuario.productsCart = this.listProductosCarro = [];
        await this._usuarioService.actualizarUsuario(this.usuario);
        this.toastr.info(this.configuracion.mensaje, this.configuracion.titulomensaje);
        this.ngxService.stop();
      }


    } catch (error) {
      this.ngxService.stop();
      this.toastr.error(error)
      console.log(error);
    }
  }

}
