import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { faCartPlus } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute } from '@angular/router';
import { ProductoDTO } from 'src/models/ProductoDTO';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { FullScreenViewer } from 'iv-viewer';
import { AlmacenamientoLocalService } from 'src/app/services/almacenamiento-local/almacenamiento-local.service';
import { datosAL } from 'src/models/datosAL';
import { UsuarioDTO, PerfilUsuario } from 'src/models/UsuarioDTO';
import { ProductShoppingCartDTO } from 'src/models/ProductShoppingCartDTO';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {

  faCartPlus = faCartPlus;

  public listProductos: ProductoDTO[] = [];
  public tipoPrecioUsuario: string = 'Lista2'; //por defecto lista2 las otras listas estan escondidas

  //para filtros en el carro de compras
  public searchTerm: string = '';
  public enOferta: boolean = null;
  public subcategoria: String = null;
  public categoriaOferta: String = null;

  //para las busqueda con algolia
  index;
  pagina: number = 0
  registrosMostrar: number = 16;

  cantidad: number = 0;
  subtotal: number = 0;

  listProductosCarro: ProductShoppingCartDTO[] = [];
  usuario: UsuarioDTO; // puede ser un cliente o un vendedor
  usuarioPuedePedir: boolean;
 
  @ViewChild('openModalButton', { static: false }) openModalButton: ElementRef;
  

  constructor(private route: ActivatedRoute,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    public _almacenamientoLocalService: AlmacenamientoLocalService,
    public _usuarioService: UsuarioService) {
  }

  ngOnInit() {

    this.scrollTop();
    this.cargarProductos();
    this.route.paramMap.subscribe(params => {

      let sesionCloseOrInit:boolean = this.route.snapshot.paramMap.get('sesion') == null || this.route.snapshot.paramMap.get('sesion') == undefined ? null :
                                      Boolean(JSON.parse(this.route.snapshot.paramMap.get('sesion')));
      console.log("sesionCloseOrInit", sesionCloseOrInit);
      console.log("this.route.snapshot.paramMap.get('sesion')", this.route.snapshot.paramMap.get('sesion'));
      if(sesionCloseOrInit != null){

        if(sesionCloseOrInit == false){
          this.usuarioPuedePedir = sesionCloseOrInit
        }
        else{
          this.getUser();

        }
      }
      else{
        this.scrollTop();
        this.subcategoria = null;
        this.categoriaOferta = null;
        this.searchTerm = '';
  
        this.subcategoria = this.route.snapshot.paramMap.get('subcategoria');
        this.categoriaOferta = this.route.snapshot.paramMap.get('categoriaOferta');
  
        console.log("sub", this.subcategoria);
        console.log("categoriaOferta", this.categoriaOferta)
  
        if (this.subcategoria != null || this.categoriaOferta != null) {
          this.cargarProductos();
        }
        else {
          this.searchTerm = params.get("filter");
          console.log("filtrando..!!");
          this.filterItems(this.searchTerm);
        }
      }
    });
    this.getUser();
  }


  filterItems(searchTerm) {

    this.ngxService.startBackground();

    this.pagina = 1;
    //se arma filtro para el scrollinfinic
    let filtro: string
    if (this.enOferta == true) {
      filtro = `habilitado = 1 AND enOferta = 1`;
    }
    else {
      filtro = `habilitado = 1`;
    }

    //se arma el objecto para hacer la consulta
    let objectQuery = {
      query: searchTerm != null && searchTerm != '' ? searchTerm.toLowerCase() : "",
      hitsPerPage: this.registrosMostrar,
      page: 0,
      filters: filtro
    }

    this.index.search(
      objectQuery
    ).then((result) => {
      this.listProductos = [];
      this.precioProductoPorUsuario(result.hits as ProductoDTO[]);
      this.ngxService.stopBackground();
    }).catch((err) => {
      this.toastr.error(err, "Ocurrio algo");
      this.ngxService.stopBackground();
    })
  }

  cargarProductos() {

    this.ngxService.startBackground();
    try {
      //se aplica la config de algolica para las busqueda

      const algoliasearch = require('algoliasearch/dist/algoliasearch.js');
      var client = algoliasearch(environment.algolia.projectKey, environment.algolia.apiKey, { protocol: 'https' })
      this.index = client.initIndex(environment.algolia.indexProduct);

      let filtro = this.subcategoria == null ? `habilitado = 1 AND enOferta = ${+this.enOferta}` : '';

      if (this.enOferta == true) {
        filtro = `habilitado = 1 AND enOferta = ${+this.enOferta}`
      }
      else if (this.subcategoria != null) {
        filtro = `habilitado = 1 AND idsubcategoria : '${this.subcategoria}'`
      }
      else if (this.categoriaOferta != null) {
        filtro = `habilitado = 1 AND idcategoriaoferta : '${this.categoriaOferta}'`
      }
      else {
        filtro = `habilitado = 1`
      }

      console.log("filtro ", filtro);

      //se arma el objecto para hacer la consulta
      let objectQuery = {
        hitsPerPage: this.registrosMostrar,
        page: 0,
        filters: filtro
      }

      //se elimina la propiedad para que no se vaya en la consulta, ya que si va nula no trae registros
      if (this.subcategoria == null && this.categoriaOferta == null && this.enOferta != true) {
        delete objectQuery.filters;
      }

      this.index.search(
        objectQuery
      ).then((result) => {
        this.listProductos = [];
        this.precioProductoPorUsuario(result.hits as ProductoDTO[]);
        this.pagina++;
        this.ngxService.stopBackground();
      }).catch((err) => {
        this.toastr.error(err, "Ocurrio algo");
        this.ngxService.stopBackground();
      });

    } catch (error) {
      this.toastr.error(error, "Ocurrio algo");
      this.ngxService.stopBackground();
    }
  }


  onScroll() {
    this.ngxService.startBackground();
    //se arma filtro para el scrollinfinic
    let filtro: string;

    if (this.enOferta == true) {
      filtro = `habilitado = 1 AND enOferta = ${+this.enOferta}`
    }
    else if (this.subcategoria != null) {
      filtro = `habilitado = 1 AND idsubcategoria : '${this.subcategoria}'`
    }
    else if (this.categoriaOferta != null) {
      filtro = `habilitado = 1 AND idcategoriaoferta : '${this.categoriaOferta}'`
    }
    else {
      filtro = `habilitado = 1`
    }

    //se arma el objecto para hacer la consulta
    let objectQuery = {
      query: this.searchTerm != null && this.searchTerm != '' ? this.searchTerm.toLowerCase() : "",
      hitsPerPage: this.registrosMostrar,
      page: this.pagina,
      filters: filtro
    }

    console.log("filtro ", filtro);

    //se elimina la propiedad para que no se vaya en la consulta, ya que si va nula no trae registros
    if (this.subcategoria == null && this.categoriaOferta == null && this.enOferta != true) {
      delete objectQuery.filters;
    }

    this.index.search(
      objectQuery
    ).then((result) => {
      if (result.hits != null && result.hits.length > 0) {
        this.precioProductoPorUsuario(result.hits as ProductoDTO[]);
        this.pagina++;
      }
      else {
        console.warn("no hay mas registros para mostrar");
      }
      this.ngxService.stopBackground();
    }).catch((err) => {
      this.toastr.error(err, "Ocurrio algo");
    });
  }

  scrollTop() {
      const contentContainer = document.querySelector('.container') || window;
      contentContainer.scrollTo(0, 0);
    //document.body.scrollTop = 0; // Safari
    //document.documentElement.scrollTop = 0; // Other
  }

  onScrollUp(){
    console.log("onScrollUp");
  }

  precioProductoPorUsuario(productosList: ProductoDTO[]) {
    if (productosList != null && productosList.length > 0) {
      productosList.forEach(element => {
        if (element.habilitado == true) {
          if (this.tipoPrecioUsuario == 'Lista2') {
            element.precio = element.lista2;
          }
          if (this.tipoPrecioUsuario == 'Lista3') {
            element.precio = element.lista3;
          }
          if (this.tipoPrecioUsuario == 'Lista4') {
            element.precio = element.lista4;
          }
          this.listProductos.push(element);
        }
      });
    }
  }

  showImageFull(image) {
    const viewer = new FullScreenViewer();
    viewer.show(image.src);
  }

  async addCart(product: ProductoDTO,index:number) {
    let datosSesion: datosAL = await this._almacenamientoLocalService.obtenerDatosSesion();
    if (datosSesion != null && datosSesion.idUsuario != null) {

      if(this.usuario != null && this.usuario.numeroCliente > 0 ){
        this.manageShoppingCart(product,index);
      }
      else{
        this.toastr.warning("Estas pendiente por aprobación.");
      }
    }
    else {
      this.openModel();
    }
  }

  async manageShoppingCart(producto: ProductoDTO,index:number) {

    let cantidadItem = document.getElementById('cantidad' + index)['value'];  
    console.log(cantidadItem);  
    if (cantidadItem == null || cantidadItem == '' || cantidadItem <= 0) {
      this.toastr.warning("Establece la cantidad.");
      return;
    }

    //verifico si el item existe
    if (this.listProductosCarro != null && this.listProductosCarro.length > 0) {
      for (let item of this.listProductosCarro) {
        if (item.producto.id == producto.id) {
          this.toastr.warning("Este producto ya existe en el carro de compras.");
          return;
        }
      }
    }

    let subtotalItem: number = producto.precio * Number(cantidadItem);
    // Falta calcular el precio dependiendo el perfil ReploiD

    //producto['cantidad'] = Number(cantidadItem);
    //producto['subtotalitem'] = subtotalItem;
    if (this.listProductosCarro == null) {
      this.listProductosCarro = [];
    }

    let productCart:ProductShoppingCartDTO = {
       cantidad :Number(cantidadItem),
       producto : producto
    }
    await this.listProductosCarro.push(productCart);

    //almacenan los items en el carro de compras
    this.usuario['productsCart'] = this.listProductosCarro;

    console.log(this.usuario)
    this._usuarioService.actualizarUsuario(this.usuario);
    //this.calcularsubtotal()
    this.toastr.info("El producto se agrego exitosamente.");

  }

  async calcularsubtotal() {
    this.subtotal = 0;
    if (this.listProductosCarro != null && this.listProductosCarro.length > 0) {
      for (let item of this.listProductosCarro) {
        let subtotalItem: number = item.producto.precio * Number(item['cantidad']);
        this.subtotal = Math.round((this.subtotal + subtotalItem)*100)/100;
      }
      console.log("this.subtotal ", this.subtotal);
    }
  }


  openModel() {
    this.openModalButton.nativeElement.click()
  }

  closeDialogLoginEvent() {
    //this.closeDialogLogin();
    //this.getUser();
  }

  getUser() {
    if(this._almacenamientoLocalService.idUsuario != null){ //cuando se va a subscribe
      this._usuarioService.getUser().subscribe(result => {
        if (this._almacenamientoLocalService.idUsuario == null || 
           this._almacenamientoLocalService.idUsuario == undefined) { //cuando se ya esta subscribe
          this.usuario = null;
          this.listProductosCarro = [];
          return;
        }
        if(result != null && result.length == 1){
          this.usuario = result[0]
          console.log("usuario",this.usuario);
          this.validarUsuario();
          if(this.usuario.productsCart == null || this.usuario.productsCart == undefined){
            this.usuario['productsCart'] = [];
          }
          this.listProductosCarro = this.usuario.productsCart;
          this.calcularsubtotal()
        }
      });
    }
  }

  /** 
   * metodo que permite establecer si el cliente o del vendedor (el vendedor siempre puede) puede hacer pedidos.
  */
 validarUsuario() {
  if (this.usuario != null &&
    this.usuario.perfil == PerfilUsuario._perfil_cliente.toString() &&
    this.usuario.numeroCliente != null &&
    this.usuario.numeroCliente > 0) { //el usuario puede pedir siempre y cuando este aprobado
    this.usuarioPuedePedir = true;
    //this.tipoPrecioUsuario = this.usuario.tipoPrecio;
  }
  else{
    this.usuarioPuedePedir = false;
  }
  console.log("this.usuarioPuedePedir", this.usuarioPuedePedir);
}


}
