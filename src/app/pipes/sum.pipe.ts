import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
   name: 'sum'
})
export class SumPipe implements PipeTransform {

   transform(items: any[], price: string, quantity?: string): any {
      let result = 0;

      items.forEach(value => {
         result += value[price] * (quantity ? value[quantity] : 1);
      });

      return result;
   }

}
