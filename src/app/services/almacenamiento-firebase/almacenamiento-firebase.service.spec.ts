import { TestBed } from '@angular/core/testing';

import { AlmacenamientoFirebaseService } from './almacenamiento-firebase.service';

describe('AlmacenamientoFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlmacenamientoFirebaseService = TestBed.get(AlmacenamientoFirebaseService);
    expect(service).toBeTruthy();
  });
});
