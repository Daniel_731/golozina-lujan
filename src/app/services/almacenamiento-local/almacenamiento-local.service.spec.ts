import { TestBed } from '@angular/core/testing';

import { AlmacenamientoLocalService } from './almacenamiento-local.service';

describe('AlmacenamientoLocalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlmacenamientoLocalService = TestBed.get(AlmacenamientoLocalService);
    expect(service).toBeTruthy();
  });
});
