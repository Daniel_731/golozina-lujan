import { Injectable } from '@angular/core';
import { UsuarioDTO } from 'src/models/UsuarioDTO';
import { datosAL } from 'src/models/datosAL';

@Injectable({
  providedIn: 'root'
})
export class AlmacenamientoLocalService {

  public idUsuario:string;
  public correoUsuario:string;
  public perfilUsuario:string;
  public nombreCompletoUsuario: string;
  public clienteTemporal: UsuarioDTO;
  
  constructor() { }

  /**
  * metodo que permirte guardar datos en  almacenamiento local (OM)
  */
  guardarDatosAL(datosal: datosAL): Promise<boolean> {
    let promesa = new Promise<boolean>((resolve, reject) => {
      //se almacenan los valores globlamente antes de guardarlos en el almacenamiento local
      this.idUsuario = datosal.idUsuario
      this.correoUsuario = datosal.correoUsuario;
      this.perfilUsuario = datosal.perfilUsuario;
      this.nombreCompletoUsuario = datosal.nombreCompleto;
      this.clienteTemporal = datosal.clienteTemporal;
      localStorage.setItem('datosSesion', JSON.stringify(datosal));
      resolve(true);
    });
    return promesa;
  }


  obtenerDatosSesion(): Promise<datosAL> {
    let promesa = new Promise<datosAL>((resolve, reject) => {
      let datosSesion:datosAL = JSON.parse(localStorage.getItem('datosSesion'));
      if (datosSesion != null && datosSesion) {
        let datosal = new datosAL();
        datosal.idUsuario = datosSesion.idUsuario
        datosal.correoUsuario = datosSesion.correoUsuario;
        datosal.perfilUsuario = datosSesion.perfilUsuario;
        datosal.nombreCompleto = datosSesion.nombreCompleto;
        datosal.clienteTemporal = datosSesion.clienteTemporal;

        this.idUsuario = datosal.idUsuario
        this.correoUsuario = datosal.correoUsuario;
        this.perfilUsuario = datosal.perfilUsuario;
        this.nombreCompletoUsuario = datosal.nombreCompleto;
        this.clienteTemporal = datosal.clienteTemporal;
        resolve(datosal);
      }
      else {
        resolve(null);
      }
    });
    return promesa;
  }

  /**
   * metodo que permite limpar TODO el almacenamiendo local (OM)
   */
  limpiarTodoAL(): void {
      localStorage.removeItem('datosSesion');
      this.idUsuario = null;
      this.correoUsuario = null;
      this.perfilUsuario = null;
      this.nombreCompletoUsuario = null;
      this.clienteTemporal = null;
      console.log('se limipio todo el almacenamiendo local');
  }

  setstorage(nombre: string, dato: any): Promise<boolean> {
    let promesa = new Promise<boolean>((resolve, reject) => {
      //se almacenan los valores globlamente antes de guardarlos en el almacenamiento local
      localStorage.setItem(nombre, dato);
      resolve(true);
    });
    return promesa;
  }

  async getstorage(nombre: string) {
    let dato = await localStorage.getItem(nombre);
    if (dato != null && dato) {
      return dato;
    }
  }
}
