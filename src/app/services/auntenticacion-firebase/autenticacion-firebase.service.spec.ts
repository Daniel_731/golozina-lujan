import { TestBed } from '@angular/core/testing';

import { AutenticacionFirebaseService } from './autenticacion-firebase.service';

describe('AutenticacionFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutenticacionFirebaseService = TestBed.get(AutenticacionFirebaseService);
    expect(service).toBeTruthy();
  });
});
