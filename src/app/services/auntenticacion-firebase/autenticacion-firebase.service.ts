import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlmacenamientoLocalService } from '../almacenamiento-local/almacenamiento-local.service';

@Injectable({
  providedIn: 'root'
})
export class AutenticacionFirebaseService {

  nombreFacebook: string;
  fotoFacebook: string;


  constructor(public afAuth: AngularFireAuth,
    public _almacenamientoLocalService: AlmacenamientoLocalService) {

    afAuth.authState.subscribe((user: firebase.User) => {
      if (!user) {
        this.nombreFacebook = null;
        return;
      }
      this.nombreFacebook = user.displayName;
      this.fotoFacebook = user.photoURL;
    });

  }

  get authenticated(): boolean {
    return this.nombreFacebook !== null;
  }

  /*crearUsuarioConFacebook(): firebase.Promise<any> {
    if (this.platform.is('cordova')) {
      return this.facebook.login(['email', 'public_profile']).then(res => {
        const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
        return this.afAuth.auth.signInWithCredential(facebookCredential);
      });
    } else {
      return this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
    }
  }*/

  crearUsuarioConEmail(correo: string, contrasena: string): Promise<boolean> {
    let promesa = new Promise<boolean>((resolve, reject) => {

      this.afAuth.auth.createUserWithEmailAndPassword(correo, contrasena)
        .then((respuesta) => {
          resolve(true);
        })
        .catch(error => {
          // Handle Errors here.
          var errorCode = error["code"];
          var errorMessage = error.message;
          if (errorCode === 'auth/wrong-password') {
            errorMessage = "Contraseña incorrecta";
          }
          else if (errorCode === 'auth/email-already-in-use') {
            errorMessage = 'El correo ya esta en uso.';
          }
          else if (errorCode === 'auth/weak-password') {
            errorMessage = 'La contraseña es muy corta.';
          }
          else {
            errorMessage = 'Error desconocido. ' + errorMessage;
          }
          console.log("error creando registrando usuario firebase" + errorMessage);
          reject(errorMessage)
        })
    });
    return promesa;
  }

  iniciarSesionConEmail(correo: string, contrasena: string): Promise<true> {

    let promesa = new Promise<true>((resolve, reject) => {

      this.afAuth.auth.signInWithEmailAndPassword(correo, contrasena)
        .then(value => {
          console.log('iniciarSesionConEmail bien');
          resolve(true);
        })
        .catch(error => {
          var errorCode = error["code"];
          var errorMessage = error.message;
          if (errorCode === 'auth/invalid-email') {
            //El correo electrónico no es valido
            errorMessage = "Usuario y/o contraseña invalidos.";
          }
          else if (errorCode === 'auth/user-disabled') {
            errorMessage = 'El usuario correspondiente al correo electrónico ha sido deshabilitado.';
          }
          else if (errorCode === 'auth/user-not-found') {
            //No hay usuario con el correo electrónico dado.
            errorMessage = 'Usuario y/o contraseña invalidos.';
          }
          else if (errorCode === 'auth/wrong-password') {
            //La contraseña no es válida.
            errorMessage = 'Usuario y/o contraseña invalidos.';
          }
          else {
            errorMessage = 'Error desconocido. ' + errorMessage;
          }
          reject(errorMessage)
        });
    });
    return promesa;
  }

  cerrarSesion() {
    let promesa = new Promise<true>((resolve, reject) => {
      this._almacenamientoLocalService.limpiarTodoAL();
      this.nombreFacebook = null;
      this.fotoFacebook = null;
      this.afAuth.auth.signOut();
      resolve(true);
    });
    return promesa;
  }


  async restrablecerContrasena(correo: string) {
    try {
      await this.afAuth.auth.sendPasswordResetEmail(correo);
      return true;
    } catch (error) {
      new error;
    }
  }

  mostrarNombreUsuario(): string {
    if (this.nombreFacebook != null) {
      return this.nombreFacebook;
    } else {
      return '';
    }
  }
}
