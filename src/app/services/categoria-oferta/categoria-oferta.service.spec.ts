import { TestBed } from '@angular/core/testing';

import { CategoriaOfertaService } from './categoria-oferta.service';

describe('CategoriaOfertaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CategoriaOfertaService = TestBed.get(CategoriaOfertaService);
    expect(service).toBeTruthy();
  });
});
