import { Injectable } from '@angular/core';
import { GenericFirestoreService } from '../genericFirestore/generic-firestore.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { CategoriaOfertaDTO } from 'src/models/CategoriaOfertaDTO';
import { ModeloFirebase } from 'src/models/ModeloFirebase';

@Injectable({
  providedIn: 'root'
})
export class CategoriaOfertaService extends GenericFirestoreService {

  constructor(public afDB: AngularFirestore) {
    super(afDB);
  }

  findCategoriasOferta(): Promise<CategoriaOfertaDTO[]> {
    return this.findRecordFirestore(ModeloFirebase.CATEGORIASOFERTA);
  }
}
