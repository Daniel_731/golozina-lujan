import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CategoriaDTO } from 'src/models/CategoriaDTO';
import { ModeloFirebase } from 'src/models/ModeloFirebase';
import { GenericFirestoreService } from '../genericFirestore/generic-firestore.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService extends GenericFirestoreService {

  constructor(public afDB: AngularFirestore) {
    super(afDB);
  }

  findCategorias(): Promise<CategoriaDTO[]> {
    return this.findRecordFirestore(ModeloFirebase.CATEGORIAS);
  }
}
