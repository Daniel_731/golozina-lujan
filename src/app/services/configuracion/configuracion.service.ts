import { Injectable } from '@angular/core';
import { GenericFirestoreService } from '../genericFirestore/generic-firestore.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModeloFirebase } from 'src/models/ModeloFirebase';
import { Configuracion } from 'src/models/Configuracion';

@Injectable({
  providedIn: 'root'
})
export class ConfiguracionService extends GenericFirestoreService {

  constructor(public afDB: AngularFirestore) {
    super(afDB);
  }

  findConfig(): Promise<Configuracion[]> {
    return this.findRecordFirestore(ModeloFirebase.CONFIGURACION);
  }
}
