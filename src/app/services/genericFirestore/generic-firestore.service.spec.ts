import { TestBed } from '@angular/core/testing';

import { GenericFirestoreService } from './generic-firestore.service';

describe('GenericFirestoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenericFirestoreService = TestBed.get(GenericFirestoreService);
    expect(service).toBeTruthy();
  });
});
