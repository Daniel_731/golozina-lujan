import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GenericFirestoreService {

  constructor(protected afDB: AngularFirestore) { 

  }

  obtenerRef(id:string,objectDbName:string) {
    return this.afDB.doc<any>(`${objectDbName}/${id}`);
  }


  obtenerCollection(objectDbName:string):AngularFirestoreCollection<any>{
    return this.afDB.collection(`/${objectDbName}`);
  }

  

  /**
   * metodo que permite consultar todos los datos de una "tabla"
   * @param objectDbName 
   */
  findRecordFirestore(objectDbName:string): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.afDB.collection(`${objectDbName}`).snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data() as any;
          const id = a.payload.doc.id;
          return { id, ...data };
        }
        ))).subscribe(datos => {
          resolve(datos)
        });
    });
  }

 /**
   * metodo que permite consultar un documento por id
   * @param id 
   * @param objectDbName 
   */
  findRecordPropertyFirestore(campo:string,valor: string,objectDbName:string): Promise<any> {

    return new Promise((resolve, reject) => {
      return this.afDB.collection(objectDbName, ref =>
      
        ref.where(campo, '==', valor)).snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            return { id, ...data };
          }
          ))).subscribe(datos => {
          if(datos != null && datos.length > 0){
            resolve(datos[0])
          }
          else{
            resolve(null);
          }
        });
    });
  }

  /**
   * 
   * @param campo 
   * @param valor 
   * @param objectDbName 
   */
  findRecordPropertyFirestoreList(campo:string,valor: string,objectDbName:string): Promise<any> {
    
    return new Promise((resolve, reject) => {
      return this.afDB.collection(objectDbName, ref =>
      
        ref.where(campo, '==', valor)).snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data() as any;
            const id = a.payload.doc.id;
            return { id, ...data };
          }
          ))).subscribe(datos => {
          if(datos != null && datos.length > 0){
            resolve(datos)
          }
          else{
            resolve(null);
          }
        });
    });
  }


  public findRecordWithMultiFilters(table: string, wheres: object[]): Promise<any> {
    try {
      return new Promise((resolve, reject) => {
          this.afDB.collection(table, ref => {
             let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

             for (let where of wheres){
              query = query.where(where['column'], where['conditional'], where['value']);
             }

             return query;
          }).snapshotChanges().pipe(
            map(actions => actions.map(a => {
              const data = a.payload.doc.data() as any;
              const id = a.payload.doc.id;
              return { id, ...data };
            }
            ))).subscribe(datos => {
            if(datos != null && datos.length > 0){
              resolve(datos)
            }
            else{
              resolve(null);
            }
          })
        });
  
    } catch (e) {
       console.error(e);
    }
 }


  /**
   * permite guarda multiples documentos
   * @param items
   * @param objectDbName 
   */
  async addOrUpdateRecordFirestoreMultiple(items:any[],objectDbName:string){

    try {

     //referecias de la bd
     let db = this.afDB.firestore;
     let batch =db.batch()

     for (let item of items) {

       if(item.id == null){
        let collectionTransaccion = db.collection(`/${objectDbName}`);
        let documento = await collectionTransaccion.add(item);
        item.id= documento.id;
        let ref = db.doc(`${objectDbName}/${documento.id}`);
        batch.update(ref, item);
       }else{
        let ref = db.doc(`${objectDbName}/${item.id}`);
        batch.update(ref, item);
       }
 
     }
     //commit
     await batch.commit();
     return true;
 
    } catch (error) {
      alert(`metodo updateRecordFirestoreMultiple ${objectDbName} error: ${error}`);
      throw new Error();
    }
 }


}
