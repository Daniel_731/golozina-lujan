import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  private hideAfterLogin: boolean;

  constructor() { }

  setHideAfterLogin(val:boolean) {
    this.hideAfterLogin = val;
  }

  getHideAfterLogin():boolean {
    return this.hideAfterLogin;
  }
}
