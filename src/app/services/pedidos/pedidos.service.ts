import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { PedidoDTO } from 'src/models/PedidoDTO';
import { PedidoDetalleDTO } from 'src/models/PedidoDetalleDTO';
import { GenericFirestoreService } from '../genericFirestore/generic-firestore.service';
import { ModeloFirebase } from 'src/models/ModeloFirebase';

@Injectable({
  providedIn: 'root'
})
export class PedidosService extends GenericFirestoreService {

  collectionPedido: AngularFirestoreCollection<PedidoDTO>;
  collectionPedidoDetalle: AngularFirestoreCollection<PedidoDetalleDTO>;

  constructor(public afDB: AngularFirestore) {
    super(afDB);
  }


  /** 
    * metodo que permite obtener pedido a procesar
   */
  obtenerDetallesPedido(idPedido: string): Promise<PedidoDetalleDTO[]> {
    return this.findRecordPropertyFirestoreList('idPedido', idPedido, ModeloFirebase.PEDIDOSDETALLE);
  }

  /** 
   * metodo que permite crear pedidos (encabezado y detalle).
  */
 async crearPedido(pedido: PedidoDTO, listDetallePedido: PedidoDetalleDTO[]){
  try {

    //referecias de la bd
    var db = this.afDB.firestore;
    var batch = db.batch();

    //se crea el pedido
    let collectionPedido = db.collection(`/${ModeloFirebase.PEDIDOS}`);

    var refPedido = collectionPedido.doc();
    pedido.id = refPedido.id;
    batch.set(refPedido, pedido);
 
    //se crean los detalles
    let collectionDetallePedido = db.collection(`/${ModeloFirebase.PEDIDOSDETALLE}`);
  
    for(let detalle of listDetallePedido) {
       var ref = collectionDetallePedido.doc();
       detalle.idPedido = refPedido.id;
       batch.set(ref, detalle);
    }

    //Commit
    await batch.commit();
    return true;

  } catch (error) {
    throw error;
  }
}

  async crearActualizarEncabezadoPedido(pedido: PedidoDTO): Promise<PedidoDTO> {
    if (pedido.id == null) { //se guarda
      let documento = await this.collectionPedido.add(pedido);
      pedido.id = documento.id;
      await this.obtenerRef(pedido.id,ModeloFirebase.PEDIDOS).update(pedido);
    }
    else { //se actualiza
      await this.obtenerRef(pedido.id,ModeloFirebase.PEDIDOS).update(pedido);
    }
    return pedido;
  }


  /**
    * Consultar pedidos por cliente
    * @param idCliente
    * @param pedidoEliminado 
    */
  consultarPedidosPorCliente(idCliente: string, pedidoEliminado: boolean = false): Promise<PedidoDTO[]> {

    let where = [
      {
        'column': 'idCliente',
        'conditional': '==',
        'value': idCliente
      },
      {
        'column': 'eliminado',
        'conditional': '==',
        'value': pedidoEliminado
      },
    ];
    return this.findRecordWithMultiFilters(ModeloFirebase.PEDIDOS,where);
  }

}

