import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModeloFirebase } from 'src/models/ModeloFirebase';
import { SubCategoriaDTO } from 'src/models/SubCategoriaDTO';
import { GenericFirestoreService } from '../genericFirestore/generic-firestore.service';

@Injectable({
  providedIn: 'root'
})
export class SubCategoriaService extends GenericFirestoreService {

  constructor(public afDB: AngularFirestore) {
    super(afDB);
  }

  findSubCategorias(): Promise<SubCategoriaDTO[]> {
    return this.findRecordFirestore(ModeloFirebase.SUBCATEGORIAS);
  }

}
