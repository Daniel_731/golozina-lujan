import { Injectable } from '@angular/core';
import { datosAL } from 'src/models/datosAL';
import { PerfilUsuario, UsuarioDTO } from 'src/models/UsuarioDTO';
import { SolicitudUsuario } from 'src/models/SolicitudUsuario';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { ModeloFirebase } from 'src/models/ModeloFirebase';
import { GenericFirestoreService } from '../genericFirestore/generic-firestore.service';
import { AutenticacionFirebaseService } from '../auntenticacion-firebase/autenticacion-firebase.service';
import { AlmacenamientoLocalService } from '../almacenamiento-local/almacenamiento-local.service';
import { AlmacenamientoFirebaseService } from '../almacenamiento-firebase/almacenamiento-firebase.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends GenericFirestoreService {

  collection: AngularFirestoreCollection<UsuarioDTO>;

  constructor(public afDB: AngularFirestore,
    public _autenticacionFirebaseService: AutenticacionFirebaseService,
    public _almacenamientoLocalService: AlmacenamientoLocalService,
    public _almacenamientoFirebaseService: AlmacenamientoFirebaseService) {
    super(afDB);

    this.collection = this.obtenerCollection(ModeloFirebase.USUARIOS);
  }


  getUser() : Observable<UsuarioDTO[]> {
    if(this._almacenamientoLocalService.idUsuario != null){
      return this.afDB.collection(ModeloFirebase.USUARIOS, ref =>
        ref.where('id', '==', this._almacenamientoLocalService.idUsuario)
      ).snapshotChanges().pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data() as UsuarioDTO;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
        ))
    }
  }

  async registrarUsuario(usuario: UsuarioDTO): Promise<boolean> {

    try {
      //se registra el usuario con autenticacion en firebase
      let registroUsuarioCorrecto: boolean =
        await this._autenticacionFirebaseService.crearUsuarioConEmail(usuario.correo, usuario.contrasena);

      if (registroUsuarioCorrecto == true) {
        //se registra el usuario
        let usuarioGuarado = await this.crearUsuario(usuario);

        //guardo los datos en el almacenamiento local
        let datosal = new datosAL();
        datosal.idUsuario = usuarioGuarado.id;
        datosal.correoUsuario = usuarioGuarado.correo;
        datosal.perfilUsuario = PerfilUsuario._perfil_cliente.toString(); //perfil de cliente
        datosal.nombreCompleto = usuarioGuarado.nombreCompleto;
        await this._almacenamientoLocalService.guardarDatosAL(datosal);

        //se estable el nombre de la imagen
        let nombreArchivo = usuario.id;
        let imagenFirebase: any = usuario.imagenFirebase;

        if (imagenFirebase != null) {
          let urlFoto: string = await this._almacenamientoFirebaseService.
            guardarFotoFirebase(imagenFirebase, nombreArchivo);

          if (urlFoto != null) {
            usuario.foto = urlFoto;
            usuario.nombreFoto = nombreArchivo;
            await this.obtenerRef(usuario.id, ModeloFirebase.USUARIOS).update(usuario);
          }
        }

        //se crea registro en modo para envio de correo.
        //cada vez que se crea un registro nuevo se envia un correo al adminstrador (firebase-functions)
        let collectionSolicitudUsuario: AngularFirestoreCollection<SolicitudUsuario> = this.obtenerCollection(ModeloFirebase.SOLICITUDUSUARIO);
        let SolicitudUsuario: SolicitudUsuario = {
          id: usuario.id,
          nombreCompleto: usuario.nombreCompleto,
          correo: usuario.correo,
          cuit: usuario.cuit,
          telefono: usuario.telefono,
          direccion: usuario.direccion,
          localidad: usuario.localidad,
          foto: usuario.foto,
          nombreFoto: usuario.nombreFoto,
          imagenFirebase: null
        }
        await collectionSolicitudUsuario.add(SolicitudUsuario);
        return true;
      }

    } catch (err) {
      throw new Error(err);
    }
  }

  async crearUsuario(usuario: UsuarioDTO): Promise<UsuarioDTO> {
    usuario.contrasena = null; //Md5.hashStr(usuario.contrasena).toString();
    let documento = await this.collection.add(usuario);
    usuario.id = documento.id;
    await this.actualizarUsuario(usuario);
    return usuario;
  }

  async actualizarUsuario(usuario: UsuarioDTO) {
    await this.obtenerRef(usuario.id, ModeloFirebase.USUARIOS).update(usuario);
  }

  consultarUsuarioPorEmail(correo: string): Promise<UsuarioDTO> {
    return this.findRecordPropertyFirestore('correo', correo, ModeloFirebase.USUARIOS);
  }

  async crearUsuarioVendedor(usuario: UsuarioDTO): Promise<boolean> {

    try {
      //se registra el usuario con autenticacion en firebase
      let registroUsuarioCorrecto: boolean =
        await this._autenticacionFirebaseService.crearUsuarioConEmail(usuario.correo, usuario.contrasena);

      if (registroUsuarioCorrecto == true) {
        let usuarioGuardado = await this.crearUsuario(usuario);
        //guardo los datos en el almacenamiento local
        return true;
      }

    } catch (err) {
      throw new Error(err);
    }
  }

  /**
   * se registra un usuario apartir de las visitas online de los vendedores
   */
  async registrarUsuarioDesdeVendedor(usuario: UsuarioDTO): Promise<boolean> {

    try {
      //se registra el usuario con autenticacion en firebase
      let registroUsuarioCorrecto: boolean =
        await this._autenticacionFirebaseService.crearUsuarioConEmail(usuario.correo, usuario.contrasena);

      if (registroUsuarioCorrecto == true) {

        //crear nodo usuario
        await this.crearUsuario(usuario);

        //se envia correo al usuario para que cambie su contraseña.
        await this._autenticacionFirebaseService.restrablecerContrasena(usuario.correo);
        return true;
      }

    } catch (err) {
      throw new Error(err);
    }
  }
}
