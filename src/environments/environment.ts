// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDyoy1i7DbvsOhCxVeB-lWKXiPzwu57SZU",
    authDomain: "golozina-lujan-de8c5.firebaseapp.com",
    databaseURL: "https://golozina-lujan-de8c5.firebaseio.com",
    projectId: "golozina-lujan-de8c5",
    storageBucket: "golozina-lujan-de8c5.appspot.com",
    messagingSenderId: "657705371860"
  },
  algolia:{
    projectKey:'BM2ZYK1281',
    apiKey: "a28c393e4d976baea92248e1336b79cd",
    indexProduct:'productos_golozina',
  },
  algolia1:{ //dat
    projectKey:'22KTX1OVR9',
    apiKey: "f3c3572d73657904b2f4cb822aaa926a",
    indexProduct:'productos_dat',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
