import { SubCategoriaDTO } from "./SubCategoriaDTO";

// Clase usada para el manejo de items en el acordeón

export class CategoriaAccordionDTO {
    public categoriaId: string;
    public categoriaNombre: string;
    public categoriaFoto?: string;
    public categoriaHabilitado: boolean;
    public open: boolean;
    public ListSubCategorias: SubCategoriaDTO[];
   
    constructor() { }
}