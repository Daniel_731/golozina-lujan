export class CategoriaOfertaDTO {
    public id?: string;
    public nombre: string;
    public foto?: string;
    public nombreFoto?: string;
    public habilitado: boolean;
    constructor() { }
}