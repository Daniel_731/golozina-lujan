import { Descuento } from './Descuento';

export class Configuracion {
    public id?: string;
    public titulomensaje: string;
    public mensaje: string;
    public descuentos?: Descuento[];
}