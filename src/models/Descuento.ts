export class Descuento {
    public tipo: string;
    public valor: number;
    public porcentaje: number;
}