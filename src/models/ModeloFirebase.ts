export class ModeloFirebase {
    public static readonly CATEGORIAS = "categorias";
    public static readonly CATEGORIASOFERTA = "categoriasofertas";
    public static readonly SUBCATEGORIAS = "subcategorias";
    public static readonly PRODUCTOS = "productos";
    public static readonly USUARIOS = "usuarios";
    public static readonly SOLICITUDUSUARIO = "solicitudusuario";
    public static readonly PEDIDOS = "pedidos";
    public static readonly PEDIDOSDETALLE = "pedidosdetalle";
    public static readonly VISITAS = "visitas";
    public static readonly CONFIGURACION = "configuracion";
}