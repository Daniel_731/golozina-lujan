export class PedidoDTO {
    public id?: string;

    public idCliente: string;
    public numCliente: number;
    public razonSocialCliente: string;
    public direccionCliente : string;
    public telefonoCliente: string;
    public correoCliente: string;
    public localidadCliente:string;
    
    public fechaPedido: string;
    public estadoPedido: string; 
    public leido:boolean; 
    public observaciones:string;
    public eliminado:boolean; 
    
    //Solo cuando el vendedor hace pedidos
    public idVendedor?: string;
    public nombreVendedor?: string;
    public correoVendedor?: string;
    constructor() { }
}

export enum EstadoPedido {
    _pedido_porprocesar = <any>"Por procesar",//(cuando el cliente hace el pedido)
    _pedido_procesado = <any>"Procesado", //cuando se procesan los pedidos.
}
