export class PedidoDetalleDTO {
    public id?: string;
    public idPedido: string;
    public idProducto: string;
    public codigoProducto: string;
    public nombreProducto: string;
    public descripcionProducto?: string;
    public fotoProducto?: string;
    public cantidad: number;
    public precio: number;    
    constructor() { }
}


