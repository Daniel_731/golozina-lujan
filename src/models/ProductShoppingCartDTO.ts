import { ProductoDTO } from './ProductoDTO';

export class ProductShoppingCartDTO {
    
    public cantidad: number;
    public producto : ProductoDTO;

    constructor(cantidad:number,producto : ProductoDTO,) {
        this.cantidad = cantidad;
        this.producto = producto;
    }
}