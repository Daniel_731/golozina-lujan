export class ProductoDTO {
    public id?: string;
    public nombre: string;
    public codigo:string;
    public nombreminuscula: string;
    public descripcion?: string;
    public lista2: number;
    public lista3: number;
    public lista4: number;
    public precio?: number;
    public idsubcategoria;
    public nombresubcategoria;
    public idcategoria?:string;
    public nombrecategoria?:string;
    public enOferta: boolean;
    public idcategoriaoferta?:string;
    public nombrecategoriaoferta?:string;
    public foto?: string;
    public nombreFoto?: string;
    public habilitado: boolean;
    public orden: number;
    constructor() { }
}