export class SolicitudUsuario {
    public id?: string;
    public nombreCompleto: string; //aqui va la razon social para los clientes.
    public correo: string;
    public cuit?: string; //solo para cliente
    public telefono: string;
    public direccion: string;
    public localidad: string;
    public foto?: string;
    public nombreFoto?: string;
    public imagenFirebase?: string; // Para la foto
    constructor() { } 
}