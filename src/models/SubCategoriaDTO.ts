export class SubCategoriaDTO {
    public id?: string;
    public idCategoria?: string;
    public categoriaNombre?: string;
    public nombre: string;
    public foto?: string;
    public nombreFoto?: string;
    public habilitado: boolean;
    constructor() { }
}