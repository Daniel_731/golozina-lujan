import { ProductShoppingCartDTO } from './ProductShoppingCartDTO';

export class UsuarioDTO {
    public id?: string;
    public nombreCompleto: string; //aqui va la razon social para los clientes.
    
    //solo para cliente (lo define el admin cuando recibe la solicitud) o el vendedor cuando crea un cliente 
    public numeroCliente?:number; 
    public tipoPrecio?:string;
    //////////////////////////////////////////////////////////

    public correo: string;
    public contrasena: string;
    public cuit?: string; //solo para cliente
    public telefono: string;
    public direccion: string;
    public localidad: string;
    public perfil?: string; //01= cliente, 02 vendedor,03 admin
    public foto?: string;
    public nombreFoto?: string;
    public habilitado?: boolean;
    public imagenFirebase?: string; // Para la foto

    //para oneSignal
    public userIdPush?: string;
    public tokenPush?: string;

    public productsCart?: ProductShoppingCartDTO[];

    
    constructor() { } 
}

export enum PerfilUsuario {
    _perfil_cliente = <any>"01",
    _perfil_vendedor = <any>"02",
    _perfil_admin = <any>"03",
    _perfil_adminsinpriv = <any>"04"
}