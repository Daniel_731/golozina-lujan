export class VisitaDTO {
    public id?: string;
    public idVendedor: string;
    public correoVendedor: string;
    public nombreVendedor: string;
    public razonSocialCliente: string;
    public numeroCliente:number; 
    public tipoPrecioCliente:string;
    public correoCliente: string;
    public cuitCliente: string;
    public telefonoCliente: string;
    public direccionCliente: string;
    public localidadCliente: string;
    public fotoCliente?: string;
    public nombreFotoCliente?: string;
    public fechaVisita: string;
    public fechaSincronizacion?: string;

    constructor() { } 
}

