import { UsuarioDTO } from "./UsuarioDTO";

export class datosAL {
    public idUsuario: string;
    public correoUsuario: string;
    public perfilUsuario: string;
    public nombreCompleto: string;
    public telefono?: string;
    public clienteTemporal?:UsuarioDTO; // Se asigna para el precio que puede ver un vendedor dependiendo el cliente seleccionado
    constructor() { }
}